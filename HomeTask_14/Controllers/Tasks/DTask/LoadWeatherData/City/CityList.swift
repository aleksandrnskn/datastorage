//
//  CityList.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 29.04.2021.
//

import Foundation

struct CitiesList: Codable {
    let name: String
    let coord: Coordinate
}

struct Coordinate: Codable {
    let lon: Double
    let lat: Double
}

class ListCities {
    
    var list: [CitiesList] = []
    
    func createList() {
        print("Creating list...")
        if let url = Bundle.main.url(forResource: "CityList", withExtension: "json") {
            
            do {
                let data = try Data(contentsOf: url)
                let city = try JSONDecoder().decode([CitiesList].self, from: data)
                self.list.append(contentsOf: city)
            }
            catch let error {
                print(error)
            }
            
        }
        print("Created.")
    }
    
    func createDict() -> [String : [Double]] {
        createList()
        var dict: [String : [Double]] = [:]
        print("Creating dict...")
        for index in 0...list.count-1 {
            var coord: [Double] = []
            coord.append(list[index].coord.lat)
            coord.append(list[index].coord.lon)
            dict.updateValue(coord, forKey: list[index].name)
        }
        print("Created.")
        return dict
    }
    
    public func getCoordinate(_ dictionary: [String : [Double]], _ city: String) -> [Double] {
        if let coordinate = dictionary[city] {
            return coordinate
        }
        return []
    }
    
    
}
