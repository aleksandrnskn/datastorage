//
//  WeatherLoaderAF.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 27.04.2021.
//

import Foundation
import Alamofire
import CoreLocation

class WeatherLoaderAF {
    
    let APIKEY = "376a9591dfe0759aa592696f963354e3"
    
    func makeDataRequest(forCoordinates coordinat: CLLocationCoordinate2D) -> String {
        guard let urlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(coordinat.latitude)&lon=\(coordinat.longitude)&appid=\(APIKEY)&units=metric".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return "Somthing wrong" }
        return urlString
    }
    
    func makeForecastDataRequest(forCoordinates coordinat: CLLocationCoordinate2D) -> String {
        guard let urlString = "https://api.openweathermap.org/data/2.5/onecall?lat=\(coordinat.latitude)&lon=\(coordinat.longitude)&exclude=minutely,alerts,hourly&units=metric&appid=\(APIKEY)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return "Somthing wrong" }
        return urlString
    }
    
    func loadCurrentWeatherAF(_ dictionary: [String : [Double]], _ city: String, completion: @escaping (CurrentWeather)  -> Void){
        
        let coordinates = ListCities().getCoordinate(dictionary, city)
        let location = CLLocationCoordinate2D(latitude: coordinates[0], longitude: coordinates[1])
        let urlString = makeDataRequest(forCoordinates: location)
        
        AF.request(urlString).responseJSON { response in
            guard let data = response.data
            else { return }
            DispatchQueue.main.async {
                
                var weatherData: CurrentWeather!
                
                do {
                    let currentWeather = try JSONDecoder().decode(CurrentWeather.self, from: data)
                    weatherData = currentWeather
                }
                catch let error {
                    print(error)
                }
                completion(weatherData)
            }
        }
    }
    
    func loadForecastFiveDaysWeatherAF(_ dictionary: [String : [Double]], _ city: String, completion: @escaping (ForecastFiveDays)  -> Void) {
        
        let coordinates = ListCities().getCoordinate(dictionary, city)
        let location = CLLocationCoordinate2D(latitude: coordinates[0], longitude: coordinates[1])
        let urlString = makeForecastDataRequest(forCoordinates: location)
        
        AF.request(urlString).responseJSON { response in
            guard let data = response.data
            else { return }
            DispatchQueue.main.async {
                
                var weatherData: ForecastFiveDays!
                
                do {
                    let currentWeather = try JSONDecoder().decode(ForecastFiveDays.self, from: data)
                    weatherData = currentWeather
                }
                catch let error {
                    print(error)
                }
                completion(weatherData)
            }
            
        }
        
    }
    
    func dateFormater (_ date: [Daily]) -> [String] {
        var dates: [String] = []
        for index in 0...date.count-1 {
            let date = Date(timeIntervalSince1970: TimeInterval(date[index].dt))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
            let dateString = dayTimePeriodFormatter.string(from: date)
            dates.append(dateString)
        }
        return dates
    }
    
}
