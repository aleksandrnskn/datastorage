//
//  WeatherData.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 27.04.2021.
//

import Foundation

struct CurrentWeather: Codable {
    
    var weather: [Weather]
    var main: Main
    var name: String
    
    init?(data: NSDictionary){
        guard let weather = data["weather"] as? [Weather],
              let main = data["main"] as? Main,
              let name = data["name"] as? String else { return nil }
        self.weather = weather
        self.main = main
        self.name = name
    }
}

struct ForecastFiveDays: Codable {
    var daily: [Daily]
    
    init?(data: NSDictionary){
        guard let daily = data["daily"] as? [Daily] else { return nil }
        self.daily = daily
    }
}

struct Weather: Codable {
    var main: String
    var description: String
    var icon: String
    
    init?(data: NSDictionary){
        guard let main = data["main"] as? String,
              let description = data["description"] as? String,
              let icon = data["icon"] as? String else { return nil }
        self.main = main
        self.description = description
        self.icon = icon
    }
}

struct Main: Codable {
    var temp: Double
    
    init?(data: NSDictionary){
        guard let temp = data["temp"] as? Double else { return nil }
        self.temp = temp
    }
}

struct Daily: Codable {
    var dt: Int
    var temp: Temp
    var weather: [Weather]
    
    init?(data: NSDictionary){
        guard let dt = data["dt"] as? Int,
              let temp = data["temp"] as? Temp,
              let weather = data["weather"] as? [Weather] else { return nil }
        self.dt = dt
        self.temp = temp
        self.weather = weather
    }
}

struct Temp: Codable {
    var day: Double
    var night: Double
    
    init?(data: NSDictionary){
        guard let day = data["day"] as? Double,
              let night = data["night"] as? Double else { return nil }
        self.day = day
        self.night = night
    }
}
