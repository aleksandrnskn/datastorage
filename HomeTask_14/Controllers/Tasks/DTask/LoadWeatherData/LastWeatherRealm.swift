//
//  LastWeatherRealm.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 07.06.2021.
//

import RealmSwift

class LastWeatherRealm {
    
    let realm = try! Realm()
    lazy var currentWetherDataRealm: Results<CurrentWeatherModel> = { self.realm.objects(CurrentWeatherModel.self) }()
    lazy var forecastWetherDataRealm: Results<ForecastWeatherModel> = { self.realm.objects(ForecastWeatherModel.self) }()
    
    func saveLastCurentWeater(_ name: String, _ temp: String, _ wDesc: String, _ wImg: String) {
        let lastCurentWeater = CurrentWeatherModel()
        lastCurentWeater.name = name
        lastCurentWeater.lastTemp = temp
        lastCurentWeater.weatherDescription = wDesc
        lastCurentWeater.weatherImage = wImg
        try! self.realm.write {
            self.realm.add(lastCurentWeater)
        }
    }
    
    func saveLastForecastWeater(_ date: String, _ wDesc: String, _ dTemp: String, _ nTemp: String, _ wImg: String) {
        let lastForecastWeater = ForecastWeatherModel()
        lastForecastWeater.dates = date
        lastForecastWeater.weatherDescription = wDesc
        lastForecastWeater.dayTemp = dTemp
        lastForecastWeater.nightTemp = nTemp
        lastForecastWeater.weatherImage = wImg
        
        try! self.realm.write {
            self.realm.add(lastForecastWeater)
        }
    }
    
    func delLastCurentWeater() {
        try! self.realm.write {
            self.realm.deleteAll()
        }
    }
}
