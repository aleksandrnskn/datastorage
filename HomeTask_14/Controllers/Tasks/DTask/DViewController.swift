//
//  DViewController.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 31.05.2021.
//

import UIKit
import CoreLocation

var loadedDictWithCoordinates: [String : [Double]] = [:]

class DViewController: UIViewController {

    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var forecastTableView: UITableView!
    
    private let locationManager = CLLocationManager()
    
    var currentWetherData: CurrentWeather!
    var forecastWetherData: ForecastFiveDays!
    var city: String = "London"
    var dates: [String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        self.locationLabel.text = LastWeatherRealm().currentWetherDataRealm.first?.name
        self.tempLabel.text = LastWeatherRealm().currentWetherDataRealm.first?.lastTemp
        self.weatherLabel.text = LastWeatherRealm().currentWetherDataRealm.first?.weatherDescription
        self.weatherImageView.image = UIImage(named: LastWeatherRealm().currentWetherDataRealm.first?.weatherImage ?? "")
        self.forecastTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.title = "Weather"
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let newDict = ListCities().createDict()
        loadedDictWithCoordinates = newDict
        LastWeatherRealm().delLastCurentWeater()
        showWeatherAF()
        self.forecastTableView.reloadData()
    }
    
    @IBAction func searchButton(_ sender: Any) {
        let dict = loadedDictWithCoordinates
        if dict.keys.contains(cityTextField.text!)  {
            city = cityTextField.text!
            LastWeatherRealm().delLastCurentWeater()
            showWeatherAF()
            cityTextField.text = ""
            cityTextField.placeholder = "Enter City"
        } else {
            cityTextField.text = ""
            cityTextField.placeholder = "City not found!"
        }
        view.endEditing(true)
    }
    
}

extension DViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dates.count != 0 {
            return dates.count
        }
        return LastWeatherRealm().forecastWetherDataRealm.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastTableViewCell", for: indexPath) as! ForecastTableViewCell
        
        cell.dateLabel.text = LastWeatherRealm().forecastWetherDataRealm[indexPath.row].dates
        cell.weatherLabel.text = LastWeatherRealm().forecastWetherDataRealm[indexPath.row].weatherDescription
        cell.dayTempLabel.text = LastWeatherRealm().forecastWetherDataRealm[indexPath.row].dayTemp
        cell.nightTempLabel.text = LastWeatherRealm().forecastWetherDataRealm[indexPath.row].nightTemp
        cell.weatherImageView.image = UIImage(named: LastWeatherRealm().forecastWetherDataRealm[indexPath.row].weatherImage)
        
        return cell
    }
}

extension DViewController {
    
    func showWeatherAF() {
        WeatherLoaderAF().loadCurrentWeatherAF(loadedDictWithCoordinates, city) { wetherData in
            self.currentWetherData = wetherData
            self.locationLabel.text = self.currentWetherData.name
            self.tempLabel.text = String(Int(self.currentWetherData.main.temp))
            self.weatherLabel.text = self.currentWetherData.weather[0].description
            self.weatherImageView.image = UIImage(named: self.currentWetherData.weather[0].icon)
            
            LastWeatherRealm().saveLastCurentWeater(self.currentWetherData.name,
                                      String(Int(self.currentWetherData.main.temp)),
                                      self.currentWetherData.weather[0].description,
                                      self.currentWetherData.weather[0].icon)
        }
        WeatherLoaderAF().loadForecastFiveDaysWeatherAF(loadedDictWithCoordinates, city) { wetherData in
            self.forecastWetherData = wetherData
            self.dates = WeatherLoaderAF().dateFormater(self.forecastWetherData.daily)
            for index in 0...self.forecastWetherData.daily.count - 1 {
                LastWeatherRealm().saveLastForecastWeater(WeatherLoaderAF().dateFormater(self.forecastWetherData.daily)[index],
                                            self.forecastWetherData.daily[index].weather[0].description,
                                            String(Int(self.forecastWetherData.daily[index].temp.day)),
                                            String(Int(self.forecastWetherData.daily[index].temp.night)),
                                            self.forecastWetherData.daily[index].weather[0].icon)
            }
            
            self.forecastTableView.reloadData()
        }
    }
}
