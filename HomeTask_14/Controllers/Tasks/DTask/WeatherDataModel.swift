//
//  WeatherDataModel.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 05.06.2021.
//

import Foundation
import RealmSwift

class CurrentWeatherModel: Object {
    
    @objc dynamic var weatherDescription = ""
    @objc dynamic var weatherImage = ""
    @objc dynamic var name = ""
    @objc dynamic var lastTemp = ""
    
}

class ForecastWeatherModel: Object {
    
    @objc dynamic var dates = ""
    @objc dynamic var weatherDescription = ""
    @objc dynamic var dayTemp = ""
    @objc dynamic var nightTemp = ""
    @objc dynamic var weatherImage = ""
}
