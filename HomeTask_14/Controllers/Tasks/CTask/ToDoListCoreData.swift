//
//  ToDoListCoreData.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 07.06.2021.
//

import UIKit
import CoreData

class ToDoListCoreData {
    
    var tasks: [ToDoListItem] = []
    
    func getData() -> [ToDoListItem] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
    
        let fetchRequest: NSFetchRequest<ToDoListItem> = ToDoListItem.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        do {
            self.tasks = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return tasks
    }
    
    func saveData(theTask task: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
    
        guard let entity = NSEntityDescription.entity(forEntityName: "ToDoListItem", in: context) else { return }
    
        let taskObject = ToDoListItem(entity: entity, insertInto: context)
        taskObject.theTask = task
        taskObject.comlieted = false
        taskObject.createDate = CViewController().date as Date
    
        do {
            try context.save()
            self.tasks.append(taskObject)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func delData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
    
        let fetchRequest: NSFetchRequest<ToDoListItem> = ToDoListItem.fetchRequest()
    
        if let tasks = try? context.fetch(fetchRequest) {
            for task in tasks {
                context.delete(task)
            }
        }
    
        do {
            try context.save()
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
