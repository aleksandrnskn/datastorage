//
//  CViewController.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 31.05.2021.
//

import UIKit

class CViewController: UIViewController {
    
    var selectedItems: [Int:Int] = [:]
    
    var tasks: [ToDoListItem] = []
    
    var date = NSDate()
    var formatDate = DateFormatter()
    
    @IBOutlet weak var toDoColectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        tasks = ToDoListCoreData().getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "ToDo List"
        
        formatDate.dateFormat = "dd.MM.YYYY"
    }

    @IBAction func addTask(_ sender: Any) {
        let newTask = UIAlertController(title: "New task", message: "Enter new task", preferredStyle: .alert)
        let saveTask = UIAlertAction(title: "Save", style: .default) { action in
            let tf = newTask.textFields?.first
            if let task = tf?.text {
                ToDoListCoreData().saveData(theTask: task)
                self.tasks = ToDoListCoreData().getData()
                self.toDoColectionView.reloadData()
            }
        }

        newTask.addTextField { _ in }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in }

        newTask.addAction(saveTask)
        newTask.addAction(cancel)

        present(newTask, animated: true, completion: nil)
    }
    
    @IBAction func delTask(_ sender: Any) {
        ToDoListCoreData().delData()
        ToDoListCoreData().tasks = []
        tasks = []
        selectedItems = [:]
        toDoColectionView.reloadData()
    }
}

extension CViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "toDoItem", for: indexPath) as! toDoItemCollectionViewCell
    
        cell.taskLabel.text = tasks[indexPath.row].theTask
        cell.dateLabel.text = formatDate.string(from: tasks[indexPath.row].createDate! as Date)
        
        if tasks[indexPath.row].comlieted {
            cell.backgroundColor = UIColor.init(displayP3Red: 0, green: 255, blue: 0, alpha: 0.2)
        } else {
            cell.backgroundColor = UIColor.init(displayP3Red: 255, green: 0, blue: 0, alpha: 0.2)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width - 16, height: 64)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedItems[indexPath.row] == indexPath.row {
            selectedItems[indexPath.row] = nil
            tasks[indexPath.row].comlieted = false
        } else {
            selectedItems[indexPath.row] = indexPath.row
            tasks[indexPath.row].comlieted = true
        }
        self.toDoColectionView.reloadData()
    }
    
}
