//
//  AViewController.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 31.05.2021.
//

import UIKit

class AViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "UserDefaults"
        
        self.nameTextField.delegate = self
        self.surnameTextField.delegate = self
        
        nameLabel.text = DefaultsPersistance.shared.userName
        surnameLabel.text = DefaultsPersistance.shared.userSurName
    }
    
}

extension AViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        DefaultsPersistance.shared.userName = nameTextField.text
        DefaultsPersistance.shared.userSurName = surnameTextField.text
        
        self.view.endEditing(true)
        return false
    }
    
}
