//
//  DefaultsPersistance.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 31.05.2021.
//

import Foundation

class DefaultsPersistance {
    static let shared = DefaultsPersistance()
    
    private let kUserNameKey = "DefaultsPersistance.kUserNameKey"
    private let kUserSurNameKey = "DefaultsPersistance.kUserSurNameKey"
    
    var userName : String? {
        set { UserDefaults.standard.set(newValue, forKey: kUserNameKey) }
        get { return UserDefaults.standard.string(forKey: kUserNameKey) }
    }
    var userSurName : String? {
        set { UserDefaults.standard.set(newValue, forKey: kUserSurNameKey) }
        get { return UserDefaults.standard.string(forKey: kUserSurNameKey) }
    }
}
