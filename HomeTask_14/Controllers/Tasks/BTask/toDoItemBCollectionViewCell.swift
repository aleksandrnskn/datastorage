//
//  toDoItemBCollectionViewCell.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 03.06.2021.
//

import UIKit

@IBDesignable class toDoItemBCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
