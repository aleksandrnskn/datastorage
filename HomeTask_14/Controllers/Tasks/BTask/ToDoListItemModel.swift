//
//  ToDoListItemModel.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 03.06.2021.
//

import RealmSwift

class ToDoListItemRealm: Object {
    
    @objc dynamic var theTask = ""
    @objc dynamic var createDate = NSDate()
    @objc dynamic var comlieted = false
    
}
