//
//  ToDoListRealm.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 07.06.2021.
//

import RealmSwift

class ToDoListRealm {
    
    let realm = try! Realm()
    lazy var tasks: Results<ToDoListItemRealm> = { self.realm.objects(ToDoListItemRealm.self) }()
    
    func save(_ task: ToDoListItemRealm) {
        print("SAVE")
        try! self.realm.write {
            self.realm.add(task)
        }
    }
    
    func del() {
        print("DEL")
        try! self.realm.write {
            self.realm.deleteAll()
        }
    }
    
    func complieted(_ index: Int, _ complite: Bool) {
        print("complieted")
        try! realm.write {
            tasks[index].comlieted = complite
        }
    }
}
