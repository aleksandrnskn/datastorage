//
//  MainCollectionViewController.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 30.05.2021.
//

import UIKit

//private let reuseIdentifier = "Cell"

private var identifier: String!

private let id = ["a": "ACollectionViewCell",
                  "b": "BCollectionViewCell",
                  "c": "CCollectionViewCell",
                  "d": "DCollectionViewCell"]

class MainCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.last {
            case 0:
                identifier = id["a"]
            case 1:
                identifier = id["b"]
            case 2:
                identifier = id["c"]
            case 3:
                identifier = id["d"]
            default:
                print(Error.self)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    
        return cell
    }
}
