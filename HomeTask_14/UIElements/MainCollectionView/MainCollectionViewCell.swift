//
//  MainCollectionViewCell.swift
//  HomeTask_14
//
//  Created by Aleksandr Aniskin on 30.05.2021.
//

import UIKit

@IBDesignable class MainCollectionViewCell: UICollectionViewCell {
    
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
}
